## Установка зависимостей

```
npm install
```

## Запуск сторибука

```
npm run storybook
```

## Запуск тестов в сторибуке

Перед запуском поднять сторибук по инструкции выше. В отдельном терминале запустить тесты в сторибуке командой

```
npm run test:storybook
```

## Запуск тестов на приложении

```
npm run test:stand
```

## Запуск allure

Если вы хотите запустить локально allure server, на вашем комьютере должна быть установлена java.

Скачать java для windows https://www.java.com/download/ie_manual.jsp

Как установить allure на windows https://www.youtube.com/watch?v=xdjN-4UxL1c&t=14s


Сервер запускается командой:

```
allure serve
``