import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';


export class RatingPage {
  private page: Page;
  public buttonSelector: string
  private positiveRatingSelector = '.rating-names_table__Jr5Mf .has-text-success'
  private tableRating = 'Рейтинг имён котиков'
  private errorAlertText = 'Ошибка загрузки рейтинга'

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
  }


  async openRatingPage() {
    return await test.step('Открываю страницу рейтинга котиков', async () => {
      await this.page.goto('/rating')
    })
  }

  getTitleEl() {
    return this.page.getByText(this.tableRating)
  }

  async getRatingTable() {
    const ratingRaw = await this.page.locator(this.positiveRatingSelector).allTextContents()
    return ratingRaw.map( v => parseInt(v))
  }

  getAlert() {
    return this.page.getByText(this.errorAlertText)
  }
}

export type RatingPageFixture = TestFixture<
RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};