import { expect, test as base } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__'

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage }) => { 
  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.abort()
    }
  );
  await ratingPage.openRatingPage()
  await expect(ratingPage.getAlert()).toBeVisible()

  /**
   * Замокать ответ метода получения рейтинга ошибкой на стороне сервера
   * Перейти на страницу рейтинга
   * Проверить, что отображается текст ошибка загрузки рейтинга
   */
});

test('Рейтинг количества лайков отображается по убыванию', async ({ page, ratingPage }) => {
  await ratingPage.openRatingPage()

  await expect(ratingPage.getTitleEl()).toBeVisible()

  const rating = await ratingPage.getRatingTable()

  const sortRating = rating.slice()
  sortRating.sort((a, b) => b - a)
  expect(rating).toEqual(sortRating)

  /**
   * Перейти на страницу рейтинга
   * Проверить, что рейтинг количества лайков отображается по убыванию
   */
});