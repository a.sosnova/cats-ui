import { NoResults } from '../no-result';

export default {
  title: 'NoResults',
  component: NoResults,
};
export const Default = {
  args: {
    text: 'Тут текст ошибки, который я хочу передать',
  },
};
